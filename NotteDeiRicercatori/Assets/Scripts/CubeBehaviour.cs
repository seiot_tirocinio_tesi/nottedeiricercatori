using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeBehaviour : MonoBehaviour
{
    private Color[] colors = { Color.blue, Color.yellow, Color.red, Color.green, };
    private int i;

    // Start is called before the first frame update
    void Start()
    {
        this.i = 0;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private GameObject CreateHologram(GameObject prefab)
    {
        var hologram = Instantiate(prefab, new Vector3(0, 0, 0.5f), Quaternion.identity);
        hologram.SetActive(true);
        hologram.GetComponent<Renderer>().material.color = colors[this.i % colors.Length];
        hologram.GetComponent<Renderer>().enabled = true;
        this.i++;
        return hologram;
    }

    public void CreateHologramWithGravity(GameObject prefab)
    {
        this.CreateHologram(prefab);
    }

    public void CreateHologramWithoutGravity(GameObject prefab)
    {
        var hologram = this.CreateHologram(prefab);
        hologram.GetComponent<Rigidbody>().useGravity = false;
        hologram.GetComponent<Rigidbody>().isKinematic = true;
    }

    public void CreateHologramWithoutGravityWithoutMass(GameObject prefab)
    {
        var hologram = this.CreateHologram(prefab);
        hologram.GetComponent<Rigidbody>().useGravity = false;
        hologram.GetComponent<Rigidbody>().isKinematic = false;
        hologram.GetComponent<Rigidbody>().mass = 1e+09f;
    }
}
